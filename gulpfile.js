/**
 *
 * Gulp task automation
 * Author Jaydn de Graaf Esquire
 * Permanentinc@gmail.com
 *
 */

'use strict';

var basePath = './',
    js       = basePath + 'js/',
    styles   = basePath + 'styles/',
    img      = basePath + 'images/',
    gulp     = require('gulp'),
    plugins  = require('gulp-load-plugins')({
        rename: {
            "gulp-css-globbing": "glob"
        }
    });

/**
 *
 * Sass Compile
 *
 */

var concatFilenames = require('gulp-concat-filenames');

var concatFilenamesOptions = {
    root   : styles + '/scss',
    prepend: '@import"',
    append : '";'
};

gulp.task('clean', function () {
    gulp.src(styles + '/css/style.min.css', {read: false})
        .pipe(plugins.clean());
});

gulp.task('glob', function () {
    gulp
        .src(styles + '**/*.scss')
        .pipe(concatFilenames('style.sass', concatFilenamesOptions))
        .pipe(gulp.dest(styles + 'scss/'));
});

gulp.task('sass', function () {

    gulp.src([styles + 'scss/style.sass'])
        .pipe(plugins.sass())
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(gulp.dest(styles + 'css'));
});

/**
 *
 * Javascript compile
 *
 */

gulp.task('thirdparty', function () {
    return gulp.src(js + 'thirdparty/*.js')
        .pipe(plugins.concat('thirdparty.js'))
        .pipe(gulp.dest(js + 'dist'))
});

/**
 *
 * Babel ES6 Transpiler
 *
 */

gulp.task('js', function () {
    return gulp.src(js + 'main.js')
        .pipe(plugins.babel())
        .pipe(gulp.dest(js + 'dist'));
});

gulp.task('homebrew', function () {
    return gulp.src(js + 'homebrew/*.js')
        .pipe(plugins.babel())
        .pipe(plugins.concat('homebrew.js'))
        .pipe(gulp.dest(js + 'dist'));
});

/**
 *
 * Uglify
 *
 */

gulp.task('distjs', function () {
    return gulp.src(js + 'main.js')
        .pipe(plugins.babel())
        .pipe(plugins.uglify())
        .pipe(gulp.dest(js + 'dist'));
});


gulp.task('disthomebrew', function () {
    return gulp.src(js + 'homebrew/*.js')
        .pipe(plugins.babel())
        .pipe(plugins.concat('homebrew.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(js + 'dist'));
});

gulp.task('distthirdparty', function () {
    return gulp.src(js + 'thirdparty/*.js')
        .pipe(plugins.concat('thirdparty.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(js + 'dist'))
});

/**
 *
 * Sprites
 *
 */

var spritesmith = require('gulp.spritesmith');
var pngquant = require('imagemin-pngquant');

gulp.task('sprites', function () {
    var spriteData = gulp.src(img + 'sprites/*.png').pipe(spritesmith({
        imgName    : 'sprites.png',
        padding    : 4,
        cssName    : '_1sprites.scss',
        cssTemplate: styles + 'scss/sprites/sprite_positions.styl.mustache'
    }));

    spriteData.img
        .pipe(plugins.imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use        : [pngquant()]
        }))
        .pipe(gulp.dest(img));

    spriteData.css
        .pipe(gulp.dest(styles + 'scss/sprites/'));


    var retinaSpriteData = gulp.src(img + 'sprites-retina/*.png').pipe(spritesmith({
        imgName    : 'sprites-retina.png',
        padding    : 8,
        cssName    : '_2sprites-retina.scss',
        cssTemplate: styles + 'scss/sprites/retina-sprite_positions.styl.mustache'
    }));

    retinaSpriteData.img
        .pipe(plugins.imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use        : [pngquant()]
        }))
        .pipe(gulp.dest(img));

    retinaSpriteData.css
        .pipe(gulp.dest(styles + 'scss/sprites/'));
});

/**
 *
 * IE8 Media Query removal
 *
 */

gulp.task('ie', function () {
    gulp.src(styles + 'css/style.min.css')
        .pipe(plugins.stripmq({width: "1024px"}))
        .pipe(plugins.rename({
            suffix: '-ie'
        }))
        .pipe(gulp.dest(styles + 'css/'))
});

/**
 *
 * Documentation
 *
 */

var gulpDoxx = require('gulp-doxx');

gulp.task('doc', function () {

    gulp.src([js + "homebrew/*.js"])
        .pipe(gulpDoxx({
            title: 'Homebrew Documentation'
        }))
        .pipe(gulp.dest('docs'));

});


/**
 *
 * Style Flow
 *
 */

var changeEvent = function (evt) {
    plugins.util.log('File', plugins.util.colors.green(evt.path.replace(new RegExp('/.*(?=/' + basePath + ')/'), '')), 'was', plugins.util.colors.green(evt.type));
};

gulp.task('default', ['clean','glob', 'sprites', 'thirdparty', 'js', 'sass', 'ie'], function () {

    gulp.watch(styles + 'scss/**/*.scss', ['sass']).on('change', function (evt) {
        changeEvent(evt);
    });

    gulp.watch(js + 'thirdparty/*.js', ['thirdparty']).on('change', function (evt) {
        changeEvent(evt);
    });

    gulp.watch(js + 'homebrew/*.js', ['homebrew']).on('change', function (evt) {
        changeEvent(evt);
    });

    gulp.watch(js + 'main.js', ['js']).on('change', function (evt) {
        changeEvent(evt);
    });

});

var gulpSequence = require('gulp-sequence');

gulp.task('deploy', gulpSequence('clean','sprites', 'distthirdparty', 'distjs', 'disthomebrew', 'sass', 'ie'));

gulp.task('home', function () {
    gulp.watch(js + 'homebrew/*.js', ['homebrew', 'doc']).on('change', function (evt) {
        changeEvent(evt);
    });
});