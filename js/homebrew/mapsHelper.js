/**
 * #Google map class helper library
 * @class Map Class for map helper
 */

/**
 * #Constructor
 *
 * Create a new Google Map object
 *
 * Example:
 *
 *      var map = GM.createMap('map-canvas', {center: GM.getLatLng(-25.363882, 131.044922), zoom: 1})
 *
 * @param {String} elementID Name of the map wrapper element
 * @param {Object} mapOptions Set of options for the map instance
 * @returns {Object} Returns a Google Map object
 */

class Map {

    constructor(ID = 'map-canvas', lat = -25.363882, lng = 131.044922, zoom = 6) {
        this.ID = ID;
        this.lat = lat;
        this.lng = lng;
        this.zoom = zoom;
        this.infowindow = null;
        this.markers = [];
        this.map = new google.maps.Map(document.getElementById(this.ID), {
            zoom  : this.zoom,
            center: this.getLatLng(this.lat, this.lng)
        });
    }


    /**
     *
     * #getLatLng
     *
     * Create a new Google LatLng object
     *
     * Example:
     *
     *      GM.getLatLng(-25.363882, 131.044922)
     *
     * @param {Integer} lat Latitude
     * @param {Integer} lng Longitude
     * @returns {Object} Returns a Google coordinate object
     */

    getLatLng(lat, lng) {
        return new google.maps.LatLng(lat, lng);
    }

    /**
     *
     * #setZoom
     *
     * Change the zoom level of a Map
     *
     * Example:
     *      map.setZoom(4)
     *
     * @param {Integer} zoomLevel Zoom leve to set the map to, higher is closer
     */

    setZoom(zoomLevel) {
        this.map.setZoom(zoomLevel);
    }

    /**
     * #panTo
     *
     * Pan a Map to a set of coordinates
     *
     * Example:
     *
     *      map.panTo(23.35234,43.34232432)
     *
     * @param lat Latitude
     * @param lng Longitude
     */

    panTo(lat, lng) {
        this.map.panTo(this.getLatLng(lat, lng));
    }

    /**
     * #fitToBounds
     *
     * Resize a Map to fit a set of marker objects
     *
     * Example:
     *
     *      map.fitToBounds(markerArray)
     *
     * @param {Array} markerArray Array of markers to fit he map to
     */

    fitToBounds(markerArray = this.markers) {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markerArray.length; i++) {
            bounds.extend(markerArray[i].position);
        }
        this.map.fitBounds(bounds);
    }

    /**
     * #addMarker
     *
     * Create a new marker object and add it to the map
     *
     *  Example:
     *
     *          map.addMarker(23.4324322,-141.543543,{title:'This is the title"}, array, callBackFunction);
     *
     * @param {Integer} lat Latitude
     * @param {Integer} lng Longitude
     * @param {Object} attributes Object of icon attributes
     * @param {Array} markerArray Array to add the markers to
     * @param {Function} callback Callback function to be used with the markerListener method
     * @returns {google.maps.Marker}
     */

    addMarker(lat, lng, attributes = {}, markerArray = this.markers, callback = this.blackhole) {
        attributes.position = this.getLatLng(lat, lng);
        attributes.map = this.map;
        var marker = new google.maps.Marker(attributes);
        this.addMarkerListener(marker, callback);
        markerArray.push(marker);
        return marker;
    }

    /**
     * * #addMarkerListener
     *
     * Add a marker listener callback function to a marker object
     *
     * @param {Object} marker Marker object to add listener to
     * @param {Function} callback Callback function to perform on click
     * @param {String} event Event type to listen to
     */
    addMarkerListener(marker, callback, event = 'click') {
        google.maps.event.addListener(marker, event, function () {
            callback(marker);
        });
    }


    /**
     *  False function to terminate flows
     */
    blackhole() {
        return;
    }


    /**
     * #geocode
     *
     * Geocoding functionality
     *
     * @param {String} value
     * @param {Function} callback
     */
    geocode(value, callback) {
        this.geocoder.geocode({'address': value + ', NZ'}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                return (!callback) ? results : callback(results);
            } else {
                return (!callback) ? undefined : callback(undefined);
            }
        });
    }

    /**
     * #infoBox
     *
     * Information box creator for marker objects
     *
     * @param {Object} marker Marker object to apply info window to
     * @param {String} content HTML to add to the infowindow
     * @param {Object} attributes Attributes to add to the infowindow
     */
    infoBox(marker, content, attributes = {}) {
        if (this.infowindow != undefined) {
            this.infowindow.close();
        }
        attributes.content = content

        this.infowindow = new google.maps.InfoWindow(attributes);
        this.infowindow.open(this.map, marker);
    }
}



