/**
 *
 * Auto Fill Form
 * Author Jaydn de Graaf Esquire
 * Permanentinc@gmail.com
 *
 */

"use strict";

jQuery(function ($) {

    if (window.location.origin === 'http://localhost') {

        $('body').append('<a href="themes/europa/styleguide.html"  style="bottom: 18px;padding: 2px 16px;text-decoration: none;opacity:0.7;outline:none;cursor:pointer;z-index:99;position:fixed;left:0;font-size:10px;border:0;background:grey;color:white;font-family:arial;">GUIDE</a>')

        $('body').append('<button id="fillform" style="opacity:0.7;outline:none;cursor:pointer;z-index:99;position:fixed;left:0;bottom:0;font-size:10px;border:0;background:grey;color:white;font-family:arial;">FILL FORM</button>')
            .on('click', '#fillform', function () {
            $.each($('input[type=text],input[type=email],textarea'), function () {
                var autoFillData = $(this).attr('data-autofill');
                if (autoFillData) {
                    $(this).val(autoFillData);
                }
            });
            $('.fancy-select option:eq(1)').prop('selected', true).trigger('change');
        });
    }

});