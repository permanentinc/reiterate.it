/**
 *
 * Facebook helper library
 *
 */

 var appID = '186052504904748';

 function Facebook(appID) {
    this.ID = appID;
    this.async();
};

/**
* Load up the Facebook Javascript SDK asynchronously
*/
Facebook.prototype.async = function () {

    var _thisID = this.ID;

    if (document.getElementById("fb-root") == null) {
        document.body.innerHTML += '<div id="fb-root"></div>';
    }

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.init({
            appId  : _thisID,
            xfbml  : true,
            version: 'v2.1'
        });
    };
}

Facebook.prototype.share = function(link){
  FB.ui({
   method: 'share_open_graph',
   action_type: 'og.likes',
   action_properties: JSON.stringify({
    object:link
})
}, function(response){
  if (response && !response['error_code']) {
      //Posting Successful
  } else {
    //Error in posting
  }
});
}

function shareToFacebook(link) {
    _fb.share(link || window.location.href)
}

