"use strict";

$(function ($) {

    function getParam(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return false;
    }

    var params = "&id=" + getParam("id");
    var key = "gkTcIgJ2Ap2zhsjCyqhXFfaWKpAWJ4F03yI7Im1ms4Amc1Fuos";
    var url = "http://api.tumblr.com/v2/blog/reiterateit.tumblr.com/posts/?api_key=" + key + params;
    var $posts = $(".posts");

    $.getJSON(url + "&callback=?", function (data) {

        $(".loading").remove();

        var posts = data.response.posts;

        for (var i = 0; i < posts.length; i++) {

            var post = posts[i];

            var shortLink = post.source.match(/\b(http|https)?(:\/\/)?(\S*)\.(\w{2,10})\b/ig)[0].replace("href=\"", "").replace("www.", "");

            var parseUrl = (function () {
                var a = document.createElement("a");
                return function (url) {
                    a.href = url;
                    return a.host;
                };
            })();

            var url = parseUrl(shortLink);

            var m = moment(post.timestamp * 1000);

            var postTmpl = "\n            <div class=\"[ post ]\" data-id=\"" + post.id + "\">\n                <div class=\"[ site ]\"><img class=\"favimage\" src=\"svgs/favimage.svg\" data-favicon=\"http://" + url + "/favicon.ico\" />" + url + "</div>\n                <p class=\"[ quote ]\">\n                    <img src=\"svgs/quotes.svg\"/>\n                    " + post.text + "\n                </p>\n                <div class=\"[ source ]\">\n                    " + post.source + "\n                    <span class=\"[ date ]\">" + m.format("MMMM D, YYYY") + "</span>\n                 </div>\n            </div>\n            ";

            $posts.append(postTmpl);
        }

        $posts.slick({
            autoplay: false,
            dots: false,
            arrows: false,
            adaptiveHeight: true
        });

        $(".favimage").each(function () {

            var $this = $(this),
                specificSrc = $this.attr("data-favicon");

            // create an img node, inject the real src attribute...
            $("<img />").bind("load", function () {

                // ...when it gets loaded [and the resource is thus available],
                // set it to the actual DOM img element.
                $this.attr("src", specificSrc);
            }).attr("src", specificSrc);
        });
    });

    $(document).on("keydown", function (e) {
        switch (e.which) {
            case 37:
                $posts.slick("slickPrev");
                break;
            case 39:
                $posts.slick("slickNext");
                break;
            default:
                return;
        }
    });

    $(".next").on("click", function () {
        $posts.slick("slickNext");
    });

    $(".prev").on("click", function () {
        $posts.slick("slickPrev");
    });

    function share(medium) {
        var text = encodeURIComponent("This is the share description");
        var url = encodeURIComponent("https://www.permanentinc.me/james");
        var title = encodeURIComponent("Some page title");

        switch (medium) {
            case "twitter":
                window.open("http://twitter.com/share?text=" + text + "&url=" + url);
                break;
            case "facebook":
                window.open("http://www.facebook.com/sharer.php?u=" + url + "&amp;t=" + text);
                break;
            case "linkedin":
                window.open("http://www.linkedin.com/shareArticle?mini=true&url=" + url + "&title=" + title + "&summary=" + text + "&source=" + window.location.origin);
                break;
            default:
                return;
        }
    }

    $(".share .button").on("click", function (e) {
        e.preventDefault();
        var medium = $(this).attr("data-medium");
        share(medium);
        console.log(medium);
    });
});