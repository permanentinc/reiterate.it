<!DOCTYPE html>
<html class="[<?php if ( isset( $_GET['id'] ) ) {
	echo ' shared ';
} ?>]">
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title>reiterate.it</title>
	<link rel="stylesheet" href="styles/css/style.min.css">
	<script src="//use.typekit.net/juu1xvg.js"></script>
	<script>try {
			Typekit.load();
		} catch (e) {
		}</script>
</head>
<body>

<header>
	<nav>
		<a href="#" class="[ logo ]"><img src="svgs/logo.svg"/></a>
		<p>Handpicked articles for product designers.</p>
		<a href="#" class="[ menu ]"><img src="svgs/triangle.svg"/></a>
	</nav>
</header>

<div class="[ wrap ]">

	<div class="[ posts ]">
		<div class="[ loading ]">
			<span class="[ ani one ]"><b></b></span>
			<span class="[ ani two ]"><b></b></span>
			<span class="[ ani three ]"><b></b></span>
			<span class="[ ani four ]"><b></b></span>
			<span class="[ ani five ]"><b></b></span>
			<span class="[ ani six ]"><b></b></span>
			<span class="[ ani seven ]"><b></b></span>
			<span class="[ ani eight ]"><b></b></span>
		</div>
	</div>

	<div class="[ share ]">
		<a href="#" data-medium="twitter" class="[ button twitter ]"><img src="svgs/twitter.svg"/></a>
		<a href="#" data-medium="facebook" class="[ button facebook ]"><img src="svgs/facebook.svg"/></a>
		<a href="#" data-medium="linkedin" class="[ button linkedin ]"><img src="svgs/linkedin.svg"/></a>
	</div>

	<div class="[ controls ]">
		<button class="[ prev ]">Previous</button>
		<button class="[ next ]">Next</button>
	</div>

	<span class="[ tip ]">Swipe left for more</span>

</div>

<script src="js/dist/jquery-1.11.2.min.js"></script>
<script src="js/dist/thirdparty.js"></script>
<script src="js/dist/main.js"></script>
<script>

</script>
</body>
</html>
